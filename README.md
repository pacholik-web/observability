# Observability

This repository have contain all elements to provide observability in my project.

## How to start

Clone this repo in to Your server. Next make sure docker has a network:

- wordproxy,
- observability.

If not, create using command:

```bash
docker network create <network_name>
```

### start docker compose

Now You can run command:

```bash
docker compose up -d
```

You cane now open `metrics.your-domain.com`
